package main

import (
	"fmt"
	"sync"
	"time"
)

func Fibonnaci(n int) int {
	if n <= 1 {
		return n
	}

	return Fibonnaci(n-1) + Fibonnaci(n-2)
}

type Function func(key int) (any, error)

type FunctionResult struct {
	value any
	err   error
}

type Memory struct {
	function Function
	cache    map[int]FunctionResult
	mux      sync.Mutex
}

func NewMemory(function Function) *Memory {
	return &Memory{
		function: function,
		cache:    make(map[int]FunctionResult),
	}
}

func (m *Memory) Get(key int) (any, error) {
	m.mux.Lock()
	result, exists := m.cache[key]
	m.mux.Unlock()

	if !exists {
		m.mux.Lock()
		result.value, result.err = m.function(key)
		m.cache[key] = result
		m.mux.Unlock()

	}

	return result.value, result.err
}

func GetFibonnaci(key int) (any, error) {
	return Fibonnaci(key), nil
}

func main() {
	cache := NewMemory(GetFibonnaci)
	fibo := []int{42, 40, 41, 42}
	var wg sync.WaitGroup

	for _, n := range fibo {
		wg.Add(1)
		go func(index int) {
			defer wg.Done()
			start := time.Now()
			value, err := cache.Get(index)
			if err != nil {
				panic(err)
			}
			fmt.Printf("%d, %s, %d\n", index, time.Since(start), value)
		}(n)
	}

	wg.Wait()
}
