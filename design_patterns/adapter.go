package main

import "fmt"

type Payment interface {
	Pay()
}

type CashPayment struct{}

func (c CashPayment) Pay() {
	fmt.Println("Payment done using Cash")
}

type BankPayment struct{}

func (b BankPayment) Pay(bankAccount int) {
	fmt.Printf("Payment done using Bank Account %d\n", bankAccount)
}

func ProcessPayment(p Payment) {
	p.Pay()
}

type BankPaymentAdapter struct {
	BankPayment *BankPayment
	BankAccount int
}

func (b BankPaymentAdapter) Pay() {
	b.BankPayment.Pay(b.BankAccount)
}

// func main() {
// 	cash := CashPayment{}
// 	ProcessPayment(cash)

// 	bank := BankPaymentAdapter{
// 		BankPayment: &BankPayment{},
// 		BankAccount: 123456,
// 	}
// 	ProcessPayment(bank)
// }
