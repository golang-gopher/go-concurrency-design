package main

import (
	"errors"
	"fmt"
)

type IProduct interface {
	SetStock(stock int)
	GetStock() int
	SetName(name string)
	GetName() string
}

type Computer struct {
	Name  string
	Stock int
}

func (c *Computer) SetStock(stock int) {
	c.Stock = stock
}

func (c *Computer) GetStock() int {
	return c.Stock
}

func (c *Computer) SetName(name string) {
	c.Name = name
}

func (c *Computer) GetName() string {
	return c.Name
}

type Laptop struct {
	Computer
}

func NewLaptop() IProduct {
	return &Laptop{
		Computer: Computer{
			Name:  "Laptop",
			Stock: 100,
		},
	}
}

type Desktop struct {
	Computer
}

func NewDesktop() IProduct {
	return &Desktop{
		Computer: Computer{
			Name:  "Desktop",
			Stock: 100,
		},
	}
}

func GetComputerFactory(computerType string) (IProduct, error) {
	if computerType == "laptop" {
		return NewLaptop(), nil
	}

	if computerType == "desktop" {
		return NewDesktop(), nil
	}

	return nil, errors.New("Invalid computer type")
}

func printNameAndStock(product IProduct) {
	fmt.Printf("Product Name: %s, with stock %d\n", product.GetName(), product.GetStock())
}

// func main() {
// 	laptop, _ := GetComputerFactory("laptop")
// 	desktop, _ := GetComputerFactory("desktop")

// 	printNameAndStock(laptop)
// 	printNameAndStock(desktop)
// }
