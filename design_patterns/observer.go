package main

import "fmt"

type Topic interface {
	Register(observer Observer)
	Broadcast()
}

type Observer interface {
	GetId() string
	UpdateValue(string)
}

//Item -> No disponible
//Item -> avise a los observadores que ya esta disponible

type Item struct {
	Observers []Observer
	name      string
	available bool
}

func NewItem(name string) *Item {
	return &Item{
		name: name,
	}
}

func (i *Item) UpdateAvailable() {
	fmt.Printf("Item %s is now available\n", i.name)
	i.available = true
	i.Broadcast()
}

func (i *Item) Broadcast() {
	for _, observer := range i.Observers {
		observer.UpdateValue(i.name)
	}
}

func (i *Item) Register(observer Observer) {
	i.Observers = append(i.Observers, observer)
}

type EmailClient struct {
	id string
}

func (eC *EmailClient) GetId() string {
	return eC.id
}

func (eC *EmailClient) UpdateValue(name string) {
	fmt.Printf("Email to %s, %s is now available\n", eC.id, name)

}

//
//func main() {
//
//	nvidiaItem := NewItem("Nvidia 3080")
//	observer1 := &EmailClient{
//		id: "12ab",
//	}
//	observer2 := &EmailClient{
//		id: "34dc",
//	}
//
//	nvidiaItem.Register(observer1)
//	nvidiaItem.Register(observer2)
//
//	nvidiaItem.UpdateAvailable()
//
//}
