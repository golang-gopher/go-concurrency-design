package main

import (
	"fmt"
	"sync"
	"time"
)

type Database struct{}

func (Database) CreateSingleConection() {
	fmt.Println("Creating Singleton Connection")
	time.Sleep(2 * time.Second)
	fmt.Println("Creation Done")
}

var db *Database
var mutex sync.Mutex

func getDatabaseInstance() *Database {
	mutex.Lock()
	if db == nil {
		fmt.Println("Creating Database Instance")
		db = &Database{}
		db.CreateSingleConection()
	} else {
		fmt.Println("Database Instance Already Created")
	}
	mutex.Unlock()
	return db
}

// func main() {
// 	var wg sync.WaitGroup

// 	for i := 0; i < 10; i++ {
// 		wg.Add(1)
// 		go func() {
// 			defer wg.Done()
// 			getDatabaseInstance()
// 		}()
// 	}

// 	wg.Wait()
// }
