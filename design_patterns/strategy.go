package main

import "fmt"

type PasswordProtector struct {
	User          string
	PasswordName  string
	HashAlgorithm HashAlgorithm
}

type HashAlgorithm interface {
	Hash(p *PasswordProtector)
}

func NewPasswordProtector(user, passwordName string, hashAlgorithm HashAlgorithm) *PasswordProtector {
	return &PasswordProtector{
		User:          user,
		PasswordName:  passwordName,
		HashAlgorithm: hashAlgorithm,
	}
}

func (p *PasswordProtector) SetHashAlgorithm(hashAlgorithm HashAlgorithm) {
	p.HashAlgorithm = hashAlgorithm
}

func (p *PasswordProtector) Hash() {
	p.HashAlgorithm.Hash(p)
}

type Sha struct{}
type Sha256 struct{}
type MD5 struct{}

func (s Sha) Hash(p *PasswordProtector) {
	fmt.Printf("Hashing using SHA for %s\n", p.PasswordName)
}

func (s Sha256) Hash(p *PasswordProtector) {
	fmt.Printf("Hashing using SHA256 for %s\n", p.PasswordName)
}

func (s MD5) Hash(p *PasswordProtector) {
	fmt.Printf("Hashing using MD5 for %s\n", p.PasswordName)
}

func main() {
	sha := new(Sha)
	md5 := new(MD5)

	passwordProtector := NewPasswordProtector("himurakenji", "admin", sha)
	passwordProtector.Hash()
	passwordProtector.SetHashAlgorithm(md5)
	passwordProtector.Hash()
}
