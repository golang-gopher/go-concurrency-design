package main

import (
	"flag"
	"fmt"
	"net"
	"os/exec"
	"regexp"
	"strconv"
	"strings"
	"sync"
)

// Flag permite obtener los argumentos de la línea de comandos
var host = flag.String("host", "scanme.nmap.org", "site to scan")

func checkEnvironment() (string, error) {
	out, err := exec.Command("ping", "-c", "1", *host).Output()

	if err != nil {
		return "", err
	}

	re := regexp.MustCompile(`ttl=(.?).[\S]`)

	ttl := fmt.Sprintf("%s", re.FindString(string(out)))

	ttl = strings.Split(ttl, "=")[1]
	ttlNum, err := strconv.Atoi(ttl)

	if err != nil {
		return "", err
	}

	if ttlNum <= 64 {
		return "\n\t[+] Linux system\n", nil
	} else if ttlNum >= 127 {
		return "\n\t[+] Windows system\n", nil
	} else {
		return "\n\t[-] the time to the life of the target system doesn't exists\n", nil
	}
}

// change to main
func main() {
	flag.Parse()

	environ, _ := checkEnvironment()
	fmt.Println(environ)

	var wg sync.WaitGroup
	protocol := "tcp"
	for i := 0; i < 65536; i++ {
		wg.Add(1)
		go func(port int) {
			defer wg.Done()
			conn, err := net.Dial(protocol, fmt.Sprintf("%s:%d", *host, port))

			if err != nil {
				// fmt.Printf("Port %d is closed\n", port)
				return
			}

			conn.Close()
			fmt.Printf("Port %d is open\n", port)
		}(i)
	}

	wg.Wait()
}
