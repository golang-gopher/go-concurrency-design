package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"net"
	"os"
)

var (
	port = flag.Int("port", 3090, "port to connect to")
	host = flag.String("host", "localhost", "host to connect")
)

// -> host:port
// write -> host:port
// read <- host:port
// > hello -> host:port -> hello

func main() {
	flag.Parse()

	// Listen on TCP port
	conn, err := net.Dial("tcp", fmt.Sprintf("%s:%d", *host, *port))

	if err != nil {
		log.Print("panic")
		log.Fatal(err)
		panic(err)
	}

	done := make(chan struct{})

	// Start a goroutine to receive data from the server
	go func() {
		io.Copy(os.Stdout, conn)
		done <- struct{}{}
	}()

	// Copy what we got to the console line
	CopyContent(conn, os.Stdin)

	conn.Close()
	<-done

}

// CopyCOntent copies content from src to dst
func CopyContent(dst io.Writer, src io.Reader) {
	_, err := io.Copy(dst, src)

	if err != nil {
		log.Fatal(err)
		panic(err)
	}

}
